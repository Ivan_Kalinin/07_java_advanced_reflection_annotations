package program;

import java.lang.reflect.Method;

import program.Secured;

public class Program {
  public static void main(String[] args) {
    try {
      Class<?> clazz = Class.forName("program.Entity");
      Method[] methods = clazz.getDeclaredMethods();
      Secured ann;

      for (Method method : methods) {
        ann = (Secured) method.getAnnotation(Secured.class);
        StringBuilder stringBuilder = new StringBuilder();
        if (ann != null) {
          stringBuilder.append("name: ").append(method.getName()).append("; annotation: ")
              .append(ann.toString()).append(" integer field: ").append(ann.integerField())
              .append("; string field: ").append(ann.stringField());
        } else {
          stringBuilder.append("name: ").append(method.getName()).append("; annotation: empty");
        }

        System.out.println(stringBuilder);
      }
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

}
