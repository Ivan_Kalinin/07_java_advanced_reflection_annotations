package program;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


@Retention(RetentionPolicy.RUNTIME)
public @interface Secured {
  int integerField();

  String stringField() default "strict";
}
