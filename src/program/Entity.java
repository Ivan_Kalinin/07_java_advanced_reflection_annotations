package program;

import program.Secured;

public class Entity {

  @Secured(integerField = 3)
  public void method1() {

  }

  @Secured(integerField = 3, stringField = "string")
  public void method2() {

  }

  private void method3() {

  }
}
